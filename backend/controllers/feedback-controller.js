const Feedback = require('../models/feedback')

const feedback = (req, res) => {
    const feedback = {
        studentNumber: req.body.studentNumber,
        studentName: req.body.studentName,
        rating: req.body.rating,
        feedback: req.body.feedback
    }
    Feedback.create(feedback)
    .then(result => {
        return res.send(result)
    })
    .catch(err => {
        return res.send(err)
    })
}

module.exports = {
    feedback
}