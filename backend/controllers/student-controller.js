const Student = require('../models/student')

const addStudent = async (req, res) => {
    const studentData = {
        studentNumber: req.body.studentNumber,
        studentName: req.body.studentName,
        degree: req.body.degree,
        course: req.body.course,
        semester: req.body.semester,
        studentEmail: req.body.studentEmail,
        mobileNumber: req.body.mobileNumber
    }
    let newStudent = new Student(studentData)
    await newStudent.save()
    res.json(newStudent)
}

const allStudents = async(req, res) => {
    Student.find()
    .then(students => {                               
        return res.send(students)        
    }) 
    .catch(err => {
        return res.send(err)
    })
}

const deleteStudent = async (req, res) => {
    Student.findByIdAndRemove({_id: req.body._id})
    .then(result => {
        return res.send(result)
    })
    .catch(err => {
        console.log(err)
    })
}

const editStudent = async (req, res) => {    
    const userData = req.body.data.student
    Student.findOneAndUpdate(
        {
            _id: req.body.data.student._id
        },
        {$set:userData},
        {upsert: false}
        )
    .then(result => {      
        return res.send(result)
    }) 
    .catch(err => {
        console.log(err)
    })
}

const getStudent = (req, res) => {
    Student.findById({_id: req.params.id})
    .then(result => {
        console.log(result)
        return res.send(result)
    })
    .catch(err => {
        console.log(err)
    })
}

module.exports = {
    addStudent,
    allStudents,
    deleteStudent,
    editStudent,
    getStudent
}
