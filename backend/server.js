const mongoose = require("mongoose");
const app = require('./express')
const config = require('./config/config')
const express = require('express')
const Schema = mongoose.Schema;
require('dotenv').config()
app.use(express.json());

mongoose.connect(process.env.mongodbUrl, {
    useNewUrlParser: true,
    useFindAndModify: false,
    useCreateIndex: true,
    useUnifiedTopology: true
}, (err) => {
     if(err) {
        console.log("error connecting mongodb -- ❌");
        console.log(err)
    } else {
        console.log("connected to mongodb -- ✅");
    }
})

app.get('/', (req, res) => {
    return res.send("Welcome to the api")
})

app.use(require('./routes/index.js'))

app.use(express.static('./public'))

app.listen(config.PORT, () => console.log(`listening on PORT ${config.PORT}`))