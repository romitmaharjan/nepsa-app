const mongoose = require('mongoose')
const Schema = mongoose.Schema

const eventSchema = new Schema({
    eventTitle: String,
    eventDate: String,
    eventTime: String,
    participants: [Schema.Types.Mixed],
    attributes: [Schema.Types.Mixed]
})

const model = mongoose.model('Event', eventSchema)

module.exports = model