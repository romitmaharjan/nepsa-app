const mongoose = require('mongoose')
const Schema = mongoose.Schema

const userSchema = new Schema({
    studentNumber: String,
    studentName: String,
    degree: String,
    course: String,
    semester: String,
    studentEmail: String,
    mobileNumber: String
});

const model = mongoose.model('Student', userSchema)

module.exports = model