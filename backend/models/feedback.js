const mongoose = require('mongoose')
const Schema = mongoose.Schema

const feedbackSchema = new Schema({
    studentNumber: String,
    studentName: String,
    rating: Number,
    feedback: String
})

const model = mongoose.model('Feedback', feedbackSchema)
module.exports = model