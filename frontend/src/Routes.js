import React, {useState} from 'react';
import {Route, Switch, BrowserRouter} from 'react-router-dom';
import ProtectedRoute from './Services/protected.route'
import PrivateRoute from './Services/private.route'
import Home from './Components/Home/Home'
import Student from './Components/Student/studentForm';
import allStudents from './Components/Student/allStudents';
import EditStudent from './Components/Student/editStudent';
import CreateAdmin from './Components/Admin/createAdmin'
import Login from './Components/Admin/login'
import Sidebar from './Components/Sidebar/sidebar'
import preRegistration from './Components/Admin/preRegistration';
import EventRegistration from './Components/event/eventRegistration'
import PostRegistration from './Components/event/postRegistration'
import ViewParticipants from './Components/event/viewParticipants'
import CreateEvent from './Components/event/createEvent'
import NewEvent from './Components/event/newEvent';
import Events from './Components/event/events'

const Routes = () => {
        return(            
            <BrowserRouter>                 
                <Switch>                                      
                    <ProtectedRoute exact path = "/" component={Home} />
                    <ProtectedRoute exact path = "/addStudent" component={Student} />
                    <ProtectedRoute exact path = "/allStudents" component={allStudents} />         
                    <ProtectedRoute exact path = "/students/edit/:id" component={EditStudent} />   
                    <PrivateRoute exact path = "/createAdmin" component={CreateAdmin} />                     
                    <ProtectedRoute exact path ="/preRegistration" component={preRegistration} />
                    <Route exact path = "/login" component={Login} /> 
                    <ProtectedRoute exact path = "/side" component={Sidebar} />
                    <ProtectedRoute exact path = "/viewParticipants" component={ViewParticipants} />
                    <Route exact path ="/eventRegistration" component={EventRegistration} />
                    <Route exact path = "/postRegistration" component={PostRegistration} />
                    <Route exact path = "/createEvent" component={CreateEvent} />
                    <Route exact path = "/events/:id" component={NewEvent} />
                    <Route exact path ="/events" component={Events} />
                    <Route path="*" component={() => "404 NOT FOUND"} />  
                </Switch>
            </BrowserRouter>
        )
    }


export default Routes;