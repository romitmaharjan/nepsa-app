import React from 'react';
import Routes from './Routes';
import './App.css';
import Notifications from 'react-notify-toast'
import 'bootstrap/dist/css/bootstrap.min.css';
import "react-datepicker/dist/react-datepicker.css";

function App() {
  return(
    <div>
      <Notifications />
       <Routes />
    </div>
  )
}

export default App;
