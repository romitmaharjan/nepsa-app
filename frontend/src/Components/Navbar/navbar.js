import React from 'react'
import {useHistory} from "react-router-dom"
import {Navbar, Nav} from 'react-bootstrap'
import {slide as Menu} from 'react-burger-menu'
import './navbar.css'
import {logout} from '../../Services/auth'

const Header = (props) => {
    const history = useHistory();
    var constant
    (localStorage.jwt === "TestLogin") ? constant = true : constant = false
    return(
        <>
       
        <Navbar id="nav" bg="dark" variant="dark" expand="lg">            
                {constant &&            
                <Menu>
            <a id="home" className="menu-item" href="/">Home</a>
            <a id="about" className="menu-item" href="/about">About</a>
            <a id="contact" className="menu-item" href="/contact">Contact</a>
        </Menu>
                }
        <Navbar.Brand href="#">
                <div className="ml-3">NEPSA App</div>            
            </Navbar.Brand>                 
            <Navbar.Toggle aria-controls = "basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
            {constant && 
                <Nav className="mr-auto justify-content-end" style={{ width: "100%" }} >
                    <Nav.Link href="/addStudent">Add Student</Nav.Link>
                    <Nav.Link href="/allStudents">All Students</Nav.Link>
                    <Nav.Link onClick={() => {
                        logout();
                        history.push('/login');                                              
                    }}>Log Out</Nav.Link>
                </Nav>
            }
            </Navbar.Collapse>                 
        </Navbar>
        </>
    )
}

export default Header;