import React, {useState} from 'react'
import './side.css'


const Sidebar = () => {
    const [addClass, setAddClass] = useState(false)

    const collapse = () => {
        const value = !addClass
        setAddClass(value)
    }

    var dNone = addClass ? 'd-none' : '';
    var container = addClass ? 'sidebar-collapsed' : '';
    var icon = addClass ? 'fa-angle-double-left fa-angle-double-right' : '';

    return(
        <div id="body-row">
            <div id ={`sidebar-container`} className={`${container} sidebar-expanded d-none d-md-block`}>
            <ul className="list-group">              
              <a href="#submenu1" data-toggle="collapse" aria-expanded="false" className="bg-dark list-group-item list-group-item-action flex-column align-items-start">
                  <div className="d-flex w-100 justify-content-start align-items-center">
                      <span className="fa fa-dashboard fa-fw mr-3">AAA</span> 
                      <span className={`menu-collapsed ${dNone}`}>Dashboard</span>
                      <span className={`submenu-icon ml-auto ${dNone}`}></span>
                  </div>
              </a>            
              <a href="#submenu2" data-toggle="collapse" aria-expanded="false" className="bg-dark list-group-item list-group-item-action flex-column align-items-start">
                  <div className="d-flex w-100 justify-content-start align-items-center">
                      <span className="fa fa-user fa-fw mr-3">AAA</span>
                      <span className={`menu-collapsed ${dNone}`}>Profile</span>
                      <span className={`submenu-icon ml-auto ${dNone}`}></span>
                  </div>
              </a>                        
              <a href="#" className="bg-dark list-group-item list-group-item-action">
                  <div className="d-flex w-100 justify-content-start align-items-center">
                      <span className="fa fa-tasks fa-fw mr-3">AAA</span>
                      <span className={`menu-collapsed ${dNone}`}>Tasks</span>    
                  </div>
              </a>                                       
              <a href="#" className="bg-dark list-group-item list-group-item-action">
                  <div className="d-flex w-100 justify-content-start align-items-center">
                      <span className="fa fa-question fa-fw mr-3">AAA</span>
                      <span className={`menu-collapsed ${dNone}`}>Help</span>
                  </div>
              </a>
              <a href="#" onClick ={collapse} data-toggle="sidebar-colapse" className="bg-dark list-group-item list-group-item-action d-flex align-items-center">
                  <div className="d-flex w-100 justify-content-start align-items-center">
                      <span id="collapse-icon" className="fa fa-2x mr-3 fa-angle-double-left">AAA</span>
                      <span id="collapse-text" className={`menu-collapsed ${dNone}`}>Collapse</span>
                  </div>
              </a>
              <li className="list-group-item logo-separator d-flex justify-content-center">
                  <img src='https://v4-alpha.getbootstrap.com/assets/brand/bootstrap-solid.svg' width="30" height="30" />    
              </li>   
          </ul>  
            </div>            
        </div>
    )
}

export default Sidebar