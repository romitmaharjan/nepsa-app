import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faHome } from "@fortawesome/free-solid-svg-icons";
import './footer.css'

const Bottom = (props) => {
    return(
        <div className="footer">            
            <FontAwesomeIcon icon = {faHome} />
        </div>
    )
}

export default Bottom;