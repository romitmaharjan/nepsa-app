import React, {useState, useEffect} from 'react'
import axios from 'axios'
import {PORT} from '../../config'
import Nav from '../Navbar/navbar'
import {Table, Button, Modal} from 'react-bootstrap'
import ViewParticipants from './viewParticipants'

const URL = PORT;

const Events = (props) => {
    let array;
    useEffect(() => {
        axios.get(`${URL}/getRegistrationDetails`)
    .then(res => {
        setEvents(res.data)
        console.log(res.data)
    })
    .catch(err => {
        console.log(err)
    })  
}, [])

    document.title = "Participants";
    const [events, setEvents] = useState([]);
    const [show, toggleShow] = useState(false)
    const [id, setId] = useState('')

    const viewParticipants = (id) => {
        setId(id);
        toggleShow(true)
    }

    const openInNewTab = (url) => {
        var win = window.open(url, '_blank');
        win.focus();
    }

    const downloadParticipants = (id) => {
        axios.get(`${URL}/downloadParticipantsDetails/${id}`)
        .then(res => {
            openInNewTab(`${URL}/downloadParticipantsDetails/${id}`);
        })
        .catch(err => {
            console.log(err)
        }) 
    }
    

    return (
        <div>
            {show && <ViewParticipants id={id} show={toggleShow} />}
            <Nav />            
            <div className="col">                
            <Table striped bordered hover responsive size="sm">
                <thead className="thead-light">
                    <tr>
                        <th>
                            Event Title
                        </th>
                        <th>
                            Date
                        </th>                       
                        <th>
                            Time
                        </th>  
                        <th>
                            Actions
                        </th>                                                                               
                    </tr>
                </thead>
                <tbody>                        
                    {events.map((event, index) => (
                        <tr key={index}>
                            <a href={"/events/"+event._id}><td value = {event._id} id = {event._id} style={{border:"none"}}>
                                {event.eventTitle}                                
                            </td></a>
                            <td>
                                {event.eventDate}
                            </td>                           
                            <td>
                                {event.eventTime}
                            </td>     
                            <td>
                                <Button variant="outline-info" size="sm" onClick={() => viewParticipants(event._id)}>View Participants</Button>
                                &nbsp;
                                <Button variant="outline-warning" size="sm" onClick={() => downloadParticipants(event._id)}>Download</Button>
                            </td>                                                  
                        </tr>
                    ))}                    
                </tbody>
            </Table>
            </div>
        </div>
    );
}

export default Events;