import React, {useState} from 'react'
import {Modal, Form, Button} from 'react-bootstrap'
import DatePicker from 'react-datepicker'
import Nav from '../Navbar/navbar'
import './eventRegistration.css'
import AddDetailsModal from './addDetailsModal'

const CreateEvent = (props) => {
    const [startDate, setStartDate] = useState(new Date())
    const [startTime, setStartTime] = useState(new Date())
    const [show, toggleShow] = useState(false)


    const handleSubmit = (event) => {
        event.preventDefault();
        toggleShow(true)
        const eventTitle = document.querySelector("#eventTitle").value
        let testDate = document.querySelector("#date").value
        let testTime = document.querySelector("#time").value        
    } 

    return(
        <>
            <Nav />
            {show && <AddDetailsModal show={toggleShow} title={document.querySelector("#eventTitle").value} date={document.querySelector("#date").value} time={document.querySelector("#time").value} />}
            <div className="text-center mb-3"><h3>Create Event</h3></div>
            <div className="registrationForm">
                <Form onSubmit={handleSubmit}>
                    <Form.Row>
                        <Form.Label>Event Title</Form.Label>
                        <Form.Control
                            type="text"
                            placeholder="Enter Event Title"
                            name="eventTitle"
                            id="eventTitle"                            
                            required
                        />
                    </Form.Row>
                    <Form.Row>
                        <Form.Label style={{marginRight: "10px"}}>Event Date</Form.Label><br />
                        <DatePicker 
                            id="date"
                            name="date"
                            className="form-control"   
                            placeholderText="Click to select a date"                         
                            selected={startDate}                           
                            onChange={date => setStartDate(date)} 
                            isClearable                           
                            showMonthDropdown
                            showYearDropdown
                            dropdownMode="select"
                            required
                        />
                    </Form.Row>
                    <Form.Row>
                        <Form.Label>Event Time</Form.Label>
                        <DatePicker 
                            id="time"
                            name="time"
                            className="form-control"
                            selected={startTime}
                            onChange={time => setStartTime(time)}
                            showTimeSelect
                            isClearable
                            showTimeSelectOnly
                            timeIntervals={15}
                            dateFormat="h:mm aa"
                            id="time"
                            name="time"
                            required
                        />
                    </Form.Row>
                    <Form.Row>
                        <Button variant="primary" type="submit">Submit</Button>
                    </Form.Row>
                </Form>
            </div>
        </>
    )
}

export default CreateEvent