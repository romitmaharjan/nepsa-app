import React, {useState} from 'react'
import {Form, Modal, Button} from 'react-bootstrap'
import axios from 'axios'
import { notify } from 'react-notify-toast'
import {PORT} from '../../config'

const PostRegistration = (props) => {   
    document.title="Successfully Registered"

    const name = props.history.location.studentName !== undefined ? props.history.location.studentName : "there";
    const studentNumber = props.history.location.studentNumber !== undefined ? props.history.location.studentNumber : "anonymous";
    const [show, setShow] = useState(true)

    const handleClose = () => setShow(false)

    const URL = PORT;

    const handleSubmit = (event) => {
        event.preventDefault();
        let rating = document.querySelector("#rating").value
        let feedback = document.querySelector("#feedback").value
        
        axios.post(`${URL}/feedback`,{
            studentName: name,
            studentNumber: studentNumber,
            rating: rating,
            feedback: feedback
        })
        .then(
            setShow(false),
            notify.show("Thank you for your feedback", "success", 3000))
        .catch(err => {
            console.log(err)
        })
    }
    return(
        <>
            <Modal            
            show={show}
            onHide={handleClose}
            backdrop="static"
            keyboard="false"
            >
                <Modal.Header closeButton>
                    <h5>Leave us a review</h5>
                </Modal.Header>
                <Modal.Body>
                    <Form style={{"padding":"0"}}>
                        <Form.Row>
                            <Form.Label>On scale of 1-5, how much would you rate the <strong>application user experience</strong>?</Form.Label>
                            <Form.Control 
                            as="select"
                            id="rating"
                            >
                                <option value="" selected disabled hidden>Select Option</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                            </Form.Control>
                        </Form.Row>
                        <Form.Row>
                            <Form.Label>Things we can improve on</Form.Label>
                            <Form.Control as="textarea" rows="3" id="feedback" />
                        </Form.Row>                        
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="warning" onClick={handleClose}>Don't want to leave a review</Button>
                    <Button variant="primary" type="Submit"  onClick={handleSubmit}>Submit</Button>                    
                </Modal.Footer>
            </Modal>
            <div className="text-center mt-5">
                <h4>Hi {name}, <br />
                Thank you for registering in NEPSA Quiz Night. We will contact you on the provided information.</h4></div>
        </>
    )
}

export default PostRegistration