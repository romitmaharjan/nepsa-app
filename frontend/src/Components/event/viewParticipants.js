import React, {useState, useEffect} from 'react'
import axios from 'axios'
import {PORT} from '../../config'
import {Table, Button, Modal} from 'react-bootstrap'

const URL = PORT;

const ViewParticipants = (props) => {
    document.title = "Participants";
    const [allParticipants, setAllParticipants] = useState([]);
    const [eventDetails, setEventDetails] = useState('');

    useEffect(() => {
        axios.get(`${URL}/getEventRegistrationAttributes/${props.id}`)
    .then(res => {
        setAllParticipants(res.data.participants)
        setEventDetails(res.data)        
    })
    .catch(err => {
        console.log(err)
    })  
}, [])

    return (
        <div>
            <Modal
            show={true}            
            size="lg"
            keyboard="false"
            backdrop="static"
            onHide={() => props.show(false)}
            >
                <Modal.Header closeButton>
                    <Modal.Title>{eventDetails.eventTitle}</Modal.Title>
                </Modal.Header>
                <Modal.Body>                    
            <Table striped bordered hover responsive size="sm">
                <thead className="thead-light">
                    <tr>
                        <th>
                            Student Number
                        </th>
                        <th>
                            Name
                        </th>                       
                        <th>
                            Student Email Address
                        </th>
                        <th>
                            Mobile Number
                        </th>                                                                                   
                    </tr>
                </thead>
                <tbody>
                    {allParticipants.map((participant, index) => (
                        <tr key={index}>
                            <td value = {participant._id} id = {participant._id}>
                                {participant.StudentNumber}                                
                            </td>
                            <td>
                                {participant.Name}
                            </td>                           
                            <td>
                                {participant.EmailAddress}
                            </td>
                            <td>
                                {participant.MobileNumber}
                            </td>                                                    
                        </tr>
                    ))}                    
                </tbody>
            </Table>            
            </Modal.Body>
            </Modal>
        </div>
    );
}

export default ViewParticipants;