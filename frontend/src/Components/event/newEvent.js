import React, {useState, useEffect} from 'react'
import {Form, Button, Modal} from 'react-bootstrap'
import axios from 'axios'
import {PORT} from '../../config'
import Header from '../Navbar/navbar'

const URL = PORT

const NewEvent = (props) => {
    const [studentNumber, setStudentNumber] = useState(true)
    const [name, setName] = useState(true)
    const [emailAddress, setEmailAddress] = useState(true)
    const [mobileNumber, setMobileNumber] = useState(true)
    const [isValid, setIsValid] = useState(false)
    const [invalidID, setInvalidID] = useState(false)
    const [eventTitle, setEventTitle] = useState('')    
    var id = props.match.params.id;
    const participant = {};

    const defaultList = ["Student Number", "Name", "Email Address", "Mobile Number"]

    useEffect(() => {
        axios.get(`${URL}/getEventRegistrationAttributes/${id}`)
        .then(res => {    
            setEventTitle(res.data.eventTitle)   
            const array = res.data.attributes            

            for(let i = 0; i < array.length; i++){  
                for(let j = 0; j < defaultList.length; j++){
                    if(array[i] === defaultList[j]){
                        setIsValid(true)                        
                        break;
                    }
                }   
                if(array[i] === "Student Number"){
                    setStudentNumber(false)                    
                }
                else if(array[i] === "Name"){
                    setName(false)
                }
                else if(array[i] === "Email Address"){
                    setEmailAddress(false)
                } 
                else if(array[i] === "Mobile Number"){
                    setMobileNumber(false)                    
                }
            }            
        })
        .catch(err => {
            console.log(err)
            if(err.response.data.error === "Invalid Event ID"){ setInvalidID(true) }
        })
    }, [])

    const handleSubmit = (event) => {
        event.preventDefault();
        
        if(!studentNumber){
            participant.StudentNumber = document.querySelector("#studentNumber").value
        }
        if(!name){
            participant.Name = document.querySelector("#name").value;
        }
        if(!emailAddress){
            participant.EmailAddress = document.querySelector("#email").value
        }
        if(!mobileNumber){
            participant.MobileNumber = document.querySelector("#mobileNumber").value
        }
        console.log(participant)

        axios.put(`${PORT}/addParticipants`,{
            participant: participant, 
            id: id
        })
        .then(res => {
            console.log(res)
        })
        .catch(err => {
            console.log(err)
        })
    }

const skeletonStudentNumber = studentNumber ? "" : <Form.Row>
                                                        <Form.Label>Student Number</Form.Label>
                                                        <Form.Control 
                                                            type="text" 
                                                            placeholder="Enter Student Number" 
                                                            name="studentNumber" 
                                                            id="studentNumber" 
                                                            required                                                            
                                                        /> 
                                                    </Form.Row>
const skeletonName = name ? "" : <Form.Row>
                                    <Form.Label>Full Name</Form.Label>
                                    <Form.Control 
                                        type="text"
                                        placeholder="Enter Full Name"
                                        name="name"
                                        id="name"
                                        required            
                                    />                            
                                </Form.Row>
const skeletonEmailAddress = emailAddress ? "" : <Form.Row>
                                                    <Form.Label>Email Address</Form.Label>
                                                    <Form.Control 
                                                        type="email"
                                                        placeholder="Enter Email Address"
                                                        name="email"
                                                        id="email"
                                                        required
                                                    />
                                                </Form.Row>
const skeletonMobileNumber = mobileNumber ? "" : <Form.Row>
                                                    <Form.Label>Mobile Number</Form.Label>
                                                    <Form.Control 
                                                        type="text"
                                                        placeholder="Enter Mobile Number"
                                                        name="mobileNumber"
                                                        id="mobileNumber"
                                                        required                                
                                                    />
                                                </Form.Row>      
                                                
const skeleton = isValid ? (<div>
                            <Header />
                            <div className="text-center mb-3"><h3>NEPSA {eventTitle} Registration</h3></div>
                            <div className="registrationForm">            
                                <Form onSubmit={handleSubmit}>
                                {skeletonStudentNumber}
                                {skeletonName}
                                {skeletonEmailAddress}
                                {skeletonMobileNumber}
                                <Form.Row className="mt-3">
                <p className="m-0 permission"><small className="text-muted">By clicking Submit, you <mark>allow</mark> NEPSA to contact you in the provided information</small></p>
                <Button type="submit" className="mt-1 w-100" variant="primary">Submit</Button>
            </Form.Row>
        </Form>
        </div>
        </div>) : ""

const invalidSkeleton = invalidID ? (<h1>Invalid Event ID</h1>) : ""

    return(
        <>
            {invalidSkeleton}
            {skeleton}                           
        </>
    )
}

export default NewEvent