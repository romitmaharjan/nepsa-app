import React, {useState} from 'react'
import axios from 'axios'
import {PORT} from '../../config'
import {Form, Button, Modal} from 'react-bootstrap'
import Header from '../Navbar/navbar'
import './eventRegistration.css'

const URL = PORT;

const EventRegistration = (props) => {
    document.title = "Event Registration"

    const [studentNumber, setStudentNumber] = useState('')
    const [studentEmail, setStudentEmail] = useState('')
    const [studentName, setStudentName] = useState('')
    const [mobileNumber, setMobileNumber] = useState('')
    const [customWords, setCustomWords] = useState('')
    const [show, setShow] = useState(false)

    const handleClose = () => setShow(false);

    const showConfirmationModal = () => setShow(true)

    const handleSubmit = (event) => {
        event.preventDefault();
        setStudentNumber(document.querySelector("#studentNumber").value)
        setStudentEmail(document.querySelector("#studentEmail").value)
        setStudentName(document.querySelector("#studentName").value)
        setMobileNumber(document.querySelector("#mobileNumber").value)
        setCustomWords(document.querySelector("#customWords").value)
        showConfirmationModal();
    }
    
    const confirmedDetails = () => {        
        axios
        .post(`${URL}/eventRegistration`, {
            studentNumber: studentNumber,
            studentName: studentName,
            studentEmail: studentEmail,
            mobileNumber: mobileNumber,
            customWords: customWords
        })
        .then(res => {
          props.history.push({
            pathname: '/postRegistration',
            studentName,
            studentNumber
        })          
        })
        .catch(err => {
            console.log(err)
        })
        setShow(false)
    }


    return(
        <>
        <Modal 
            show={show}
            onHide={handleClose}
            backdrop="static"
            keyboard="false"    
        >
            <Modal.Header closeButton>
                <Modal.Title>Confirm Details</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <span><strong>Student Number:</strong> {studentNumber}</span><br />
                <span><strong>Name:</strong> {studentName}</span><br />
                <span><strong>Email Address:</strong> {studentEmail}</span><br />
                <span><strong>Mobile Number:</strong> {mobileNumber}</span><br />
                <span><strong>Words of your Choice:</strong> {customWords}</span>
            </Modal.Body>
            <Modal.Footer>
                <p><small className="text-muted">P.S. We will not spam you</small></p>
                <Button variant="danger" onClick={handleClose}>
                    Make Changes
                </Button>
                <Button variant="success" onClick={confirmedDetails}>
                   Submit
                </Button>
            </Modal.Footer>
        </Modal>
        <Header />
        <div className="text-center mb-3"><h3>NEPSA Pictionary Registration</h3></div>
        <div className="registrationForm">            
        <Form onSubmit={handleSubmit}>
            <Form.Row>
                <Form.Label>Student Number</Form.Label>
                <Form.Control 
                type="text"
                placeholder="Enter Student Number"
                name="studentNumber"
                id="studentNumber"
                required
                defaultValue={studentNumber}
                />
            </Form.Row>
            <Form.Row>
                <Form.Label>Full Name</Form.Label>
                <Form.Control 
                type="text"
                placeholder="Enter Full Name"
                name="studentName"
                id="studentName"
                required
                defaultValue={studentName}
                />
            </Form.Row>
            <Form.Row>
                <Form.Label>Email Address</Form.Label>
                <Form.Control 
                type="email"
                placeholder="Enter Email Address"
                name="studentEmail"
                id="studentEmail"
                required
                defaultName={studentEmail}
                />
            </Form.Row>
            <Form.Row>
                <Form.Label>Mobile Number</Form.Label>
                <Form.Control 
                type="text"
                placeholder="Enter Mobile Number"
                name="mobileNumber"
                id="mobileNumber"
                required
                defaultValue={mobileNumber}
                />
            </Form.Row>
            <Form.Row>
                <Form.Label>Words of your choice</Form.Label>
                <Form.Control 
                as="textarea"
                placeholder="Separate using a comma (,)"
                name="customWords"
                id="customWords"
                required
                defaultValue={customWords}
                />
            </Form.Row>
            <Form.Row className="mt-3">
                <p className="m-0 permission"><small className="text-muted">By clicking Submit, you <mark>allow</mark> NEPSA to contact you in the provided information</small></p>
                <Button type="submit" className="mt-1 w-100" variant="primary">Submit</Button>
            </Form.Row>
        </Form>
        </div>
        </>
    )
}

export default EventRegistration