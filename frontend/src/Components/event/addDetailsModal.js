import React, {useState} from 'react'
import {Form, Modal, Button, Collapse, ListGroup} from 'react-bootstrap'
import {useHistory} from "react-router-dom"
import axios from 'axios'
import { notify } from 'react-notify-toast'
import {PORT} from '../../config'
import './eventRegistration.css'

const URL = PORT;

const AddDetailsModal = (props) => {
    const [selected, setSelected] = useState(true)
    const [defaultOpen, setDefaultOpen] = useState(true)
    const [customOpen, setCustomOpen] = useState(false)
    const [toggleCustom, setToggleCustom] = useState(true)
    const defaultList= ["Student Number", "Name", "Email Address", "Mobile Number"]
    let newList
    let customList = []
    const history = useHistory();

    const handleClose = () => props.show(false);

    const handleCustomOptions = () => setToggleCustom(!toggleCustom)

    const selectCustom = () => {
        setSelected(false)
        setDefaultOpen(false)
        setCustomOpen(true)
    }

    const selectDefault = () => {
        setSelected(true)
        setDefaultOpen(true)
        setCustomOpen(false)
    }

    const handleCustomChange = (event) => {
        const target = event.target
        const value = target.value

        if(target.checked){
            customList.push(value)
        } else {
            customList.pop(value)
        }
    }

    const handleSubmit = () => {
        let eventTitle = props.title
        let date = props.date
        let time = props.time

        if(selected==true){
            newList = defaultList
        } else {
            newList = customList
        }

        axios
        .post(`${URL}/eventRegistration`,{
            eventTitle: eventTitle,
            eventDate: date,
            eventTime: time,
            newList
        })
        .then(res => {
            notify.show("Event created Successfully", "success", 5000)
            history.push({
                pathname: `/events/${res.data._id}`                             
            })
            console.log(res)
        })
        .catch(err => {
            console.log(err)
        })

        handleClose();        
    }    
    
    return(
        <>
        <Modal 
            show={true}
            onHide={handleClose}
            backdrop="static"
            keyboard="false"
        >
            <Modal.Header closeButton>
                <Modal.Title>Event Details: {props.title} {props.date} {props.time}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form className="p-0">
                    <input 
                        type="radio" 
                        id="default" 
                        value="default" 
                        name="radio" 
                        checked={selected}                         
                        onChange={() => selectDefault()}
                         />
                    <label for="default" className="radioLabel">Default</label>        
                    <Collapse in={defaultOpen}>
                        <div id="example-collapse-text">
                            <ListGroup className="listgroup">
                                {defaultList.map(a => (
                                    <ListGroup.Item key={a}>                    
                                        <Form className="p-0">
                                            <Form.Check                                                 
                                                type="switch"
                                                id="custom-switch"
                                                label={a}
                                                checked
                                                disabled
                                            />
                                        </Form>
                                    </ListGroup.Item>
                                ))}
                            </ListGroup>
                        </div>
                    </Collapse><br />
                    <input type="radio" 
                    id="custom" 
                    value="custom" 
                    name="radio" 
                    onChange={() => selectCustom()}
                    />
                    <label for="custom" className="radioLabel">Custom</label>
                    <Collapse in={customOpen}>
                        <div id="example-collapse-text">
                        <Form className="p-0">
                            <ListGroup className="listgroup">
                                {defaultList.map(a => (
                                    <ListGroup.Item key={a}>                    
                                            <Form.Switch                                                 
                                                id={`switch-${a}`}
                                                label={a}
                                                type="switch"
                                                value={`${a}`}   
                                                onChange={handleCustomChange}                                                 
                                            />
                                    </ListGroup.Item>
                                ))}
                            </ListGroup>
                            </Form>
                        </div>
                    </Collapse><br />
                </Form>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="success" type="submit" onClick={handleSubmit}>Submit</Button>
            </Modal.Footer>
        </Modal>
        </>
    )
}

export default AddDetailsModal
