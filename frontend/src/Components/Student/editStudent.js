import React, {useState, useEffect} from 'react'
import axios from 'axios'
import {PORT} from '../../config'
import {Form, Button} from 'react-bootstrap'
import { notify } from 'react-notify-toast'
import './studentForm.css'
import Nav from '../Navbar/navbar'
import ExpiredInformation from '../Admin/expiredInformation'

const URL = PORT;

const EditStudent = (props) => {
    document.title = "Edit Student";
    const [student, setStudent] = useState([]);
    const [expired, setExpired] = useState(false)
    const token = "Bearer " + localStorage.getItem('token')
    var id = props.match.params.id;

    useEffect(() => {
        axios.get(`${URL}/getStudent/${id}`, {
            headers:{
                'authorization': token
            }
            })
    .then(result => {
        setStudent(result.data);
    })
    .catch(err => {
        if(err.response !== undefined){      
            if(err.response.data.error === "Token expired"){            
                localStorage.removeItem('token')
                setExpired(true)
            }
        }        
    })
    }, [])

    const handleSubmit = (event) => {             
        event.preventDefault();
        const studentNumber = document.querySelector("#studentNumber").value
        const studentName = document.querySelector("#studentName").value
        const degree = document.querySelector("#degree").value
        const course = document.querySelector("#course").value
        const semester = document.querySelector("#semester").value
        const email = document.querySelector("#email").value
        const mobileNumber = document.querySelector("#mobileNumber").value

        const updatedStudent = {"studentNumber": studentNumber, "studentName": studentName, "degree": degree, "course": course, 
                                "semester": semester, "studentEmail": email, "mobileNumber": mobileNumber, "_id": id}

        axios
        .put(`${URL}/updateStudent`, 
        {data: {student: updatedStudent},
        headers:{Authorization: "token"}})
        .then(res => {
            console.log(res);
            notify.show("Successfully Updated", "success", 3000);    
            props.history.push('/allStudents')                  
        })
        .catch(err => { 
            console.log(err)
        });        
    }

    return(      
        <div>
            {expired && <ExpiredInformation /> }
            <Nav />
        <div className="studentForm">
        <Form name="Form" onSubmit={handleSubmit}>
            
                <Form.Group>
                    <Form.Label>Student Number</Form.Label>
                    <Form.Control                          
                        required   
                        id="studentNumber"                   
                        name="studentNumber"
                        type="number"
                        placeholder="Enter Student Number"
                        defaultValue={student.studentNumber}                                                
                    />
                </Form.Group>

            
                <Form.Group>
                    <Form.Label>Full Name</Form.Label>
                    <Form.Control  
                        required    
                        id="studentName"                   
                        name = "name"
                        type="text"
                        placeholder="Enter Full Name"
                        defaultValue={student.studentName}                        
                    />
                </Form.Group>
            
                <Form.Group>
                    <Form.Label>Select Degree</Form.Label>
                    <Form.Control 
                        as="select"  
                        id="degree"                                               
                        >
                        <option value={student.degree} selected disabled>{student.degree}</option>
                        <option value="Diploma in">Diploma in</option>
                        <option value="Bachelor of">Bachelor of</option>
                        <option value="Master of">Master of</option>
                        <option value="phD in">phD in</option>
                    </Form.Control>
                </Form.Group>

                <Form.Group>
                    <Form.Label>Course</Form.Label>                                           
                    <Form.Control 
                        id="course"  
                        required                      
                        name="course"
                        type="text"
                        placeholder="Enter Course"
                        defaultValue={student.course}                    
                    />                    
                </Form.Group>
                        
                <Form.Group>
                    <Form.Label>Semester</Form.Label>
                    <Form.Control
                        as="select"
                        id="semester"                                                                                                                      
                        >
                        <option value={student.semester} selected disabled>{student.semester}</option>
                        <option value="First">First</option>
                        <option value="Second">Second</option>
                        <option value="Third">Third</option>
                        <option value="Fourth">Fourth</option>
                        <option value="Fifth">Fifth</option>
                        <option value="Sixth">Sixth</option>
                    </Form.Control>
                </Form.Group>
            
            
                <Form.Group>
                    <Form.Label>Student Email Address</Form.Label>
                    <Form.Control  
                        required      
                        id="email"                 
                        name="email"
                        type="email"
                        placeholder="Enter Student Email Address"
                        defaultValue={student.studentEmail}                       
                    />
                </Form.Group>

                <Form.Group>
                    <Form.Label>Mobile Number</Form.Label>
                    <Form.Control  
                        required    
                        id="mobileNumber"                   
                        name="mobileNumber"
                        type="number"
                        placeholder="Enter Mobile Number"
                        defaultValue={student.mobileNumber}                       
                    />
                </Form.Group>
            
                <Button type="submit" size="lg" block>Submit</Button>            
            
        </Form>
        </div>
        </div>
    )
}

export default EditStudent;