import React, {useState} from 'react'
import axios from 'axios'
import {PORT} from '../../config'
import {Form, Button, Modal, InputGroup} from 'react-bootstrap'
import { notify } from 'react-notify-toast'
import './studentForm.css'
import Nav from '../Navbar/navbar'
import ExpiredInformation from '../Admin/expiredInformation'

const URL = PORT;
console.log(URL);

const Student = (props) => {
    document.title = "Add Student";
    const token = "Bearer " + localStorage.getItem('token')
    const [expired, setExpired] = useState(false)

    const [studentNumber, setStudentNumber] = useState('')
    const [studentName, setStudentName] = useState('')
    const [degree, setDegree] = useState('');
    const [finalDegree, setFinalDegree] = useState('');
    const [course, setCourse] = useState('')
    const [semester, setSemester] = useState('')
    const [studentEmail, setStudentEmail] = useState('')
    const [mobileNumber, setMobileNumber] = useState('')
 
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);

    const showConfirmationModal = () => setShow(true)

    const resetForm = () => {
        document.querySelector("#studentNumber").value = ""
        document.querySelector("#studentName").value = ""
        document.querySelector("#degree").value = ""
        document.querySelector("#inlineFormInputGroup").value =""
        document.querySelector("#semester").value = ""
        document.querySelector("#email").value = ""
        document.querySelector("#mobileNumber").value = ""
    }

    const handleSubmit = (event) => {             
        event.preventDefault();
        setStudentNumber(document.querySelector("#studentNumber").value)
        setStudentName(document.querySelector("#studentName").value)
        setFinalDegree(document.querySelector("#degree").value)
        setCourse(document.querySelector("#inlineFormInputGroup").value)
        setSemester(document.querySelector("#semester").value)
        setStudentEmail(document.querySelector("#email").value)
        setMobileNumber(document.querySelector("#mobileNumber").value)
        showConfirmationModal();            
    }

    const confirmDetails = () => {
        axios
        .post(`${URL}/addStudent`, {
            studentNumber: studentNumber,
            studentName: studentName,
            degree: finalDegree,
            course: course,
            semester: semester,
            studentEmail: studentEmail,
            mobileNumber: mobileNumber
        }, {
            headers:{
                'authorization': token
            }
            })
        .then(res => {
            console.log(res);
            notify.show("Successfully added", "success", 3000);                    
        })
        .catch(err => { 
            if(err.response !== undefined){      
                if(err.response.data.error === "Token expired"){            
                    localStorage.removeItem('token')
                    setExpired(true)
                }
            }
        });   
        setShow(false)  
        resetForm()      
    }

    return(      
        <div>
            {expired && <ExpiredInformation /> }
            <Modal
                show={show}
                onHide={handleClose}
                backdrop="static"
                keyboard="false"
                >
                    <Modal.Header closeButton>
                        <Modal.Title>Confirm Details</Modal.Title>                                        
                    </Modal.Header>
                    <Modal.Body>
                        <span><strong>Student Number:</strong> {studentNumber}</span><br />
                        <span><strong>Name:</strong> {studentName}</span><br />
                        <span><strong>Course:</strong> {degree + " " + course}</span><br />
                        <span><strong>Semester:</strong> {semester}</span><br />
                        <span><strong>Student Email Address:</strong> {studentEmail}</span><br />
                        <span><strong>Mobile Number:</strong> {mobileNumber}</span>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="danger" onClick={handleClose}>
                            Make Changes
                        </Button>
                        <Button variant="success" onClick={confirmDetails}>
                            Submit
                        </Button>
                    </Modal.Footer>
                </Modal>
            <Nav />
        <div className="studentForm">
        <Form name="Form" onSubmit={handleSubmit}>
            
                <Form.Group>
                    <Form.Label>Student Number</Form.Label>
                    <Form.Control  
                        id="studentNumber"                        
                        required                      
                        name="studentNumber"
                        type="number"
                        placeholder="Enter Student Number"
                        defaultValue={studentNumber}                        
                    />
                </Form.Group>
            
            
                <Form.Group>
                    <Form.Label>Full Name</Form.Label>
                    <Form.Control  
                        id="studentName"
                        required                       
                        name = "name"
                        type="text"
                        placeholder="Enter Full Name"
                        defaultValue={studentName}
                    />
                </Form.Group>
            
                <Form.Group>
                    <Form.Label>Select Degree</Form.Label>
                    <Form.Control 
                        id="degree"
                        as="select" 
                        onChange={e=> setDegree(e.target.value)}
                        required>
                        <option value="" selected disabled hidden>Select Degree</option>
                        <option value="Diploma in">Diploma</option>
                        <option value="Bachelor of">Bachelor</option>
                        <option value="Master of">Master</option>
                        <option value="phD in">phD</option>
                    </Form.Control>
                </Form.Group>

                <Form.Group>
                    <Form.Label>Course</Form.Label> 
                    <InputGroup className="mb-2">
                    <InputGroup.Prepend>
                        <InputGroup.Text>{degree}</InputGroup.Text>
                    </InputGroup.Prepend>                   
                    <Form.Control 
                        id="inlineFormInputGroup"  
                        required                      
                        name="course"
                        type="text"
                        placeholder="Enter Course"
                        defaultValue={course}
                    />
                    </InputGroup>
                </Form.Group>
                        
                <Form.Group>
                    <Form.Label>Semester</Form.Label>
                    <Form.Control
                        id="semester"
                        as="select" 
                        required                                                                     
                        >
                        <option value="" selected disabled hidden>Select Semester</option>
                        <option value="First">First</option>
                        <option value="Second">Second</option>
                        <option value="Third">Third</option>
                        <option value="Fourth">Fourth</option>
                        <option value="Fifth">Fifth</option>
                        <option value="Sixth">Sixth</option>
                    </Form.Control>
                </Form.Group>
            
            
                <Form.Group>
                    <Form.Label>Student Email Address</Form.Label>
                    <Form.Control  
                        id="email"
                        required                       
                        name="email"
                        type="email"
                        placeholder="Enter Student Email Address"
                        defaultValue={studentEmail}
                    />
                </Form.Group>

                <Form.Group>
                    <Form.Label>Mobile Number</Form.Label>
                    <Form.Control 
                        id="mobileNumber" 
                        required                       
                        name="mobileNumber"
                        type="number"
                        placeholder="Enter Mobile Number"
                        defaultValue={mobileNumber}
                    />
                </Form.Group>
            
                <Button type="submit" size="lg" block>Submit</Button>            
            
        </Form>
        </div>
        </div>
    )
}

export default Student;