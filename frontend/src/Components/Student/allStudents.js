import React, {useState, useEffect} from 'react'
import axios from 'axios'
import {PORT} from '../../config'
import './studentForm.css'
import Nav from '../Navbar/navbar'
import Sidebar from '../Sidebar/sidebar'
import {Table, Button, Modal} from 'react-bootstrap'
import ExpiredInformation from '../Admin/expiredInformation'

const URL = PORT;

const AllStudents = (props) => {
    const token = "Bearer " + localStorage.getItem('token')
    const [expired, setExpired] = useState(false)

    useEffect(() => {
        axios.get(`${URL}/allStudents`, {
            headers:{
                'authorization': token
            }
        })
    .then(res => {
        setAllStudents(res.data)        
    })
    .catch(err => {  
        if(err.response !== undefined){      
        if(err.response.data.error === "Token expired"){            
            localStorage.removeItem('token')
            setExpired(true)
        }
    }
    })  
}, [])

    document.title = "All Students";
    const [allStudents, setAllStudents] = useState([]);
    const [deleteId, setDeleteId] = useState('');
    const [studentNumber, setStudentNumber] = useState('');
    const [studentName, setStudentName] = useState('');

    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);

    const deleteStudent = (_id) => {
        const id = _id           
        const arr = allStudents.filter(q => q._id !== id)
        setAllStudents(arr)
        
        axios.delete(`${URL}/deleteStudent`, 
            {data: {_id: _id},
                    headers:{Authorization: "token"}})
        .then(res => {
            console.log(res)
        })
        .catch(err => {
            console.log(err)
        })
    }

    const showConfirmationModal = (id, number, name) => {
        setShow(true)
        setDeleteId(id)
        setStudentNumber(number)
        setStudentName(name)
    };

    var link = "/students/edit/";

    return (
        <div>
            {expired && <ExpiredInformation /> }
            <Nav />            
            <Modal
                size="sm"
                show={show}
                onHide={handleClose}
                backdrop="static"
                keyboard="false"
            >
                <Modal.Header closeButton>
                    <Modal.Title style={{fontSize: "16px"}}>Delete {studentName}<br />{"("+studentNumber+") "}?</Modal.Title>
                </Modal.Header>
                <Modal.Footer>
                    <Button variant="primary" onClick={handleClose}>No</Button>
                    <Button variant="danger" onClick={() => {deleteStudent(deleteId); handleClose();}}>Yes</Button>
                </Modal.Footer>
            </Modal>        
            <div className="col">                
            <Table striped bordered hover responsive size="sm">
                <thead className="thead-light">
                    <tr>
                        <th>
                            Student Number
                        </th>
                        <th>
                            Name
                        </th>
                        <th>
                            Course
                        </th>
                        <th>
                            Semester
                        </th>
                        <th>
                            Student Email Address
                        </th>
                        <th>
                            Mobile Number
                        </th>
                        <th>
                            Update
                        </th>                                    
                    </tr>
                </thead>
                <tbody>
                    {allStudents.map((student, index) => (
                        <tr key={index}>
                            <td value = {student._id} id = {student._id}>
                                {student.studentNumber}                                
                            </td>
                            <td>
                                {student.studentName}
                            </td>
                            <td>
                                {student.degree == null ? "" : student.degree + " "}{student.course}
                            </td>
                            <td>
                                {student.semester}
                            </td>
                            <td>
                                {student.studentEmail}
                            </td>
                            <td>
                                {student.mobileNumber}
                            </td>
                            <td>
                                <a href={link+student._id}><Button variant="outline-success" size="sm">Edit</Button></a>{' '}
                                <Button id={student._id} variant="outline-danger" onClick={() => showConfirmationModal(student._id, student.studentNumber, student.studentName)} size="sm">Delete</Button>                             
                            </td>
                        </tr>
                    ))}                    
                </tbody>
            </Table>
            </div>
        </div>
    );
}

export default AllStudents;