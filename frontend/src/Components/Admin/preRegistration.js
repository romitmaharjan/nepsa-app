import React from 'react'
import {Button, Form} from 'react-bootstrap'
import Header from '../Navbar/navbar'
import {correctCode} from '../../Services/auth'
import { notify } from 'react-notify-toast'

const preRegistration = (props) => {

    const handleSubmit = (event) => {
        event.preventDefault();
        let code = document.querySelector("#code").value

        if(code === "Z3kR0m") {
            correctCode()
            props.history.push("/createAdmin")            
        } else {
            notify.show("Code does not match", "error", 4000)
        }
    }

    return(        
        <div>
            <Header />
            <div className="studentForm">
                <Form onSubmit={handleSubmit}>
                    <Form.Group>
                        <Form.Label>Code</Form.Label>
                        <Form.Control
                            required
                            name="code"
                            id="code"
                            placeholder="Enter Provided Code"
                            type="text"
                        />
                    </Form.Group>                
                    <Button type="submit">Submit</Button>
                </Form>                
            </div>
        </div>
    )
}

export default preRegistration