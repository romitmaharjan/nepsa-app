import React from 'react'
import axios from 'axios'
import {PORT} from '../../config'
import {Form, Button} from 'react-bootstrap'
import '../Student/studentForm.css'
import Nav from '../Navbar/navbar'
import {removeCode} from '../../Services/auth'

const URL = PORT;

const CreateAdmin = (props) => {
    removeCode()
    const handleSubmit = (event) =>{
        event.preventDefault();
        let email = document.querySelector("#email").value;
        let password = document.querySelector("#password").value;
        axios
        .post(`${URL}/createAdmin`, {
            email: email,
            password: password,
            isActive: true
        })
        .then(result => console.log(result),
        props.history.push("/login")
        )
        .catch(err => console.log(err))
    }    

    return(
        <div>
            <Nav />
            <div className="studentForm">
                <Form onSubmit={handleSubmit}>
                    <Form.Group>
                        <Form.Label>Email Address</Form.Label>
                        <Form.Control
                            required
                            name="email"
                            id="email"
                            placeholder="Enter Email Address"
                            type="email"
                        />
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Password</Form.Label>
                        <Form.Control
                            required
                            name="password"
                            id="password"
                            placeholder="Enter Password"
                            type="password"
                        />
                    </Form.Group>
                    <Button type="submit">Submit</Button>
                </Form>                
            </div>
        </div>
    )
}

export default CreateAdmin