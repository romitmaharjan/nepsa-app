import React from 'react'
import axios from 'axios'
import {PORT} from '../../config'
import {Form, Button} from 'react-bootstrap'
import '../Student/studentForm.css'
import Header from '../Navbar/navbar'
import { notify } from 'react-notify-toast'
import {login} from '../../Services/auth'

const URL = PORT;

const Login = (props) => {
    const handleSubmit = (event) =>{
        event.preventDefault();
        let email = document.querySelector("#email").value;
        let password = document.querySelector("#password").value;

        axios
        .post(`${URL}/login`, {
            email: email,
            password: password,
        })
        .then(res => {
            const token = res.data.accessToken; 
            login();                    
            localStorage.setItem('token', token)  
            props.history.push("/allStudents")
        }            
        )
        .catch(err => {
        if(err.response !== undefined){
        if(err.response.data.error === "Email not found"){
            notify.show("Email not found", "error", 4000)
        } else if(err.response.data.error === "Incorrect password"){
            notify.show("Passwords do not match", "warning", 4000)
        } else if(err.response.data.error === "User is not active"){
            notify.show("Please verify your email", "warning", 4000)
        }
    }
    }
        )
    }    

    return(
        <div>
            <Header />
            <div className="studentForm">
                <Form onSubmit={handleSubmit}>
                    <Form.Group>
                        <Form.Label>Email Address</Form.Label>
                        <Form.Control
                            required
                            name="email"
                            id="email"
                            placeholder="Enter Email Address"
                            type="email"
                        />
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Password</Form.Label>
                        <Form.Control
                            required
                            name="password"
                            id="password"
                            placeholder="Enter Password"
                            type="password"
                        />
                    </Form.Group>
                    <Button type="submit">Submit</Button>
                </Form>                
            </div>
        </div>
    )
}

export default Login