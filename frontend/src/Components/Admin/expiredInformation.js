import React, {useState, useEffect} from 'react'
import {useHistory} from 'react-router-dom'
import {Modal, Button} from 'react-bootstrap'
import {logout} from '../../Services/auth'

const ExpiredInformation = (props) => {

    const history = useHistory();
    function handleClick() {
        localStorage.removeItem("token")
        logout();
        history.push('/login');
    }

    return(
        <Modal
            show={true}
            backdrop="static"
            keyboard={false}>

            <Modal.Header>
                <Modal.Title>Login Expired</Modal.Title>                
            </Modal.Header>
            <Modal.Body>
                    Please login again to use the app
            </Modal.Body>
            <Modal.Footer>
                <Button variant="success" onClick={() => handleClick()}>Go back to login page</Button>
            </Modal.Footer>
        </Modal>
    )
}

export default ExpiredInformation