import React from 'react'
import Header from '../Navbar/navbar'
import Student from '../Student/studentForm'
import Bottom from '../Footer/footer'
import './Home.css'

const Home = (props) => {
    document.title = "NEPSA App";
    return(
        <div>
            <Header />
            <div className="container-fluid">        
                <Student/>
            </div>
            <Bottom />
        </div>
    )
}

export default Home;