import React from "react";
import { Route, Redirect } from "react-router-dom";
import {isCorrect} from "./auth";

const PrivateRoute = ({component: Component, ...rest}) => {
    return (

        // Show the component only when the user is logged in
        // Otherwise, redirect the user to /signin page
        <Route {...rest} render={props => (
            isCorrect() ?
                <Component {...props} />
            : <Redirect to="/preRegistration" />
        )} />
    );
};

export default PrivateRoute;
