const TOKEN_KEY = 'jwt';
const code = "code"

export const login = () => {
    localStorage.setItem(TOKEN_KEY, 'TestLogin');
}

export const logout = () => {
    localStorage.removeItem(TOKEN_KEY); 
    localStorage.removeItem('token')   
}

export const correctCode = () => {
    localStorage.setItem(code, 'Code')
}

export const removeCode = () => {
    localStorage.removeItem(code);
}

export const isLogin = () => {
    if (localStorage.getItem(TOKEN_KEY)) {
        return true;
    }

    return false;
}

export const isCorrect = () => {
    if(localStorage.getItem(code)){
        return true
    } 

    return false
}